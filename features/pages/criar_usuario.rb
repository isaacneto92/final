class User < SitePrism::Page
    set_url '/users/new'
    element :nome, '#user_name'
    element :sobrenome, '#user_lastname'
    element :email, '#user_email'
    element :endereco, '#user_address'
    element :universidade, '#user_university'
    element :profissao, '#user_profile'
    element :genero, '#user_gender'
    element :idade, '#user_age'
    
    
    element :criar, 'input[value="Criar"]'


    def Preencher_usuario
        nome.set'isaac'
        sobrenome.set'neto'
        email.set'isaac@gmail.com'
        endereco.set'praca'
        universidade.set'pedhdhdhj'
        profissao.set'QA'
        genero.set'masculino'
        idade.set'29'
        criar.click
    end

end